/* eslint-disable */
//
// to run this file use "node examples.js" in terminal
//
const unixtime = require('./src/unixtime');
const datestring = require('./src/datestring');
const company = require('./src/company');
const sum = require('./src/sum');

console.log('unixtime in milliseconds', unixtime(true));
console.log('unixtime in seconds', unixtime(false));

console.log('datestring with format "dddd, MMMM Do YYYY, h:mm:ss a" -> ', datestring('dddd, MMMM Do YYYY, h:mm:ss a'));
console.log('datestring with format "ddd, hA" -> ', datestring('ddd, hA'));

console.log('sum(5, 7)', sum(5, 7));

console.log('company(128)', company(128));
console.log('company(cat)', company(cat));
