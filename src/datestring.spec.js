const unixtime = require('./unixtime');
const datestring = require('./datestring');

jest.mock('./unixtime');

unixtime.mockReturnValue(1559677465);

describe('datestring test', function() {    
    it ('datestring dddd, MMMM Do YYYY, h:mm:ss a', function() {
        expect(datestring('dddd, MMMM Do YYYY, h:mm:ss a')).toBe('Tuesday, June 4th 2019, 7:44:25 pm');
    });
    it ('datestring ddd, hA', function() {
        expect(datestring('ddd, hA')).toBe('Tue, 7PM');
    });
    it ('datestring dd.mm.yyyy', function() {
        expect(datestring('DD.MM.YYYY')).toBe('04.06.2019');
    });
    it('error "no format given"', function() {
        expect(function() {
            datestring(); 
        }).toThrow('No format given');
    });
});