const company = require('./company');

describe('company', function() {
    it('error when input is string', function() {
        expect(function() {
            company('cat'); 
        }).toThrow('id needs to be integer');
    });
});

describe('company snapshot test', function() {
    it('company id 3', function() {
        expect(company(3)).toMatchSnapshot();
    });
    it('company id 85', function() {
        expect(company(85)).toMatchSnapshot();
    });
});   